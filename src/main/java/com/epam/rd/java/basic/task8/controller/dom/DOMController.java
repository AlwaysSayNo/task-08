package com.epam.rd.java.basic.task8.controller.dom;

import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private String xsdFileName;

	private static final String FLOWERS = "flowers", FLOWER = "flower", NAME = "name", SOIL = "soil", ORIGIN = "origin",
			VISUAL_PARAMETERS = "visualParameters", STEM_COLOUR = "stemColour", LEAF_COLOUR = "leafColour",
			AVE_LEN_FLOWER = "aveLenFlower", GROWING_TIPS = "growingTips", TEMPRETURE = "tempreture",
			LIGHTING = "lighting", WATERING = "watering", MULTIPLYING = "multiplying",
			MEASURE = "measure", LIGHT_REQUIRING = "lightRequiring";


	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public DOMController(String xmlFileName, String xsdFileName) {
		this.xmlFileName = xmlFileName;
		this.xsdFileName = xsdFileName;
	}

	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void setXsdFileName(String xsdFileName) {
		this.xsdFileName = xsdFileName;
	}

	public List<Flower> parseFlowers() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		factory.setNamespaceAware(true);

		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new File(xmlFileName));
		document.getDocumentElement().normalize();

		if(xsdFileName != null) validate(document);

		Element root = document.getDocumentElement();
		NodeList flowersNL = root.getElementsByTagName("flower");

		List<Flower> flowers = new ArrayList<>();
		for(int i = 0; i < flowersNL.getLength(); ++i){
			Node node = flowersNL.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element fElement = (Element) node;

				Flower flower = new Flower();

				flower.setName(getNameFromElement(fElement));
				flower.setSoil(getSoilFromElement(fElement));
				flower.setOrigin(getOriginFromElement(fElement));
				flower.setVisualParameters(getVisualParametersFromElement(fElement));
				flower.setGrowingTips(getGrowingTipsFromElement(fElement));
				flower.setMultiplying(getMultiplyingFromElement(fElement));

				flowers.add(flower);
			}
		}

		return flowers;
	}

	public static void writeFlowers(List<Flower> flowers, String outputFileName) throws ParserConfigurationException {
		Document document = createXMLDocument(flowers);
		try(FileOutputStream output = new FileOutputStream(outputFileName)){
			writeXML(document, output);
		} catch (IOException | TransformerException e) {
			e.printStackTrace();
		}
	}

	private static Document createXMLDocument(List<Flower> flowers) throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document document = builder.newDocument();

		Element root = document.createElement(FLOWERS);
		root.setAttribute( "xmlns","http://www.nure.ua");
		root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
		document.appendChild(root);

		Element element;
		for(Flower f : flowers){
			Element flower = document.createElement(FLOWER);

			element = document.createElement(NAME);
			element.setTextContent(f.getName());
			flower.appendChild(element);

			element = document.createElement(SOIL);
			element.setTextContent(f.getSoil());
			flower.appendChild(element);

			element = document.createElement(ORIGIN);
			element.setTextContent(f.getOrigin());
			flower.appendChild(element);

			Element visualParameters = document.createElement(VISUAL_PARAMETERS);
			element = document.createElement(STEM_COLOUR);
			element.setTextContent(f.getVisualParameters().getStemColour());
			visualParameters.appendChild(element);
			element = document.createElement(LEAF_COLOUR);
			element.setTextContent(f.getVisualParameters().getLeafColour());
			visualParameters.appendChild(element);
			element = document.createElement(AVE_LEN_FLOWER);
			element.setTextContent(Integer.toString(f.getVisualParameters().getAveLenFlower()));
			element.setAttribute(MEASURE, f.getVisualParameters().getAveLenFlowerMeasure());
			visualParameters.appendChild(element);
			flower.appendChild(visualParameters);

			Element growingTips = document.createElement(GROWING_TIPS);
			element = document.createElement(TEMPRETURE);
			element.setTextContent(Integer.toString(f.getGrowingTips().getTempreture()));
			element.setAttribute(MEASURE, f.getGrowingTips().getTempretureMeasure());
			growingTips.appendChild(element);
			element = document.createElement(LIGHTING);
			element.setAttribute(LIGHT_REQUIRING, f.getGrowingTips().getLightRequiring());
			growingTips.appendChild(element);
			element = document.createElement(WATERING);
			element.setTextContent(Integer.toString(f.getGrowingTips().getWatering()));
			element.setAttribute(MEASURE, f.getGrowingTips().getWateringMeasure());
			growingTips.appendChild(element);
			flower.appendChild(growingTips);

			element = document.createElement(MULTIPLYING);
			element.setTextContent(f.getMultiplying());
			flower.appendChild(element);

			root.appendChild(flower);
		}

		return document;
	}

	private static void writeXML(Document document, OutputStream output) throws TransformerException {
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer();

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result);
	}

	private String getNameFromElement(Element fElement) {
		return fElement.getElementsByTagName(NAME).item(0).getTextContent();
	}

	private String getSoilFromElement(Element fElement) {
		return fElement.getElementsByTagName(SOIL).item(0).getTextContent();
	}

	private String getOriginFromElement(Element fElement) {
		return fElement.getElementsByTagName(ORIGIN).item(0).getTextContent();
	}

	private VisualParameters getVisualParametersFromElement(Element fElement) {
		VisualParameters vp = new VisualParameters();
		Element vpElement = (Element) fElement.getElementsByTagName(VISUAL_PARAMETERS).item(0);

		vp.setStemColour(vpElement.getElementsByTagName(STEM_COLOUR).item(0).getTextContent());
		vp.setLeafColour(vpElement.getElementsByTagName(LEAF_COLOUR).item(0).getTextContent());
		vp.setAveLenFlower(Integer.parseInt(
				vpElement.getElementsByTagName(AVE_LEN_FLOWER).item(0).getTextContent())
		);

		Element avElement = (Element) vpElement.getElementsByTagName(AVE_LEN_FLOWER).item(0);
		vp.setAveLenFlowerMeasure(avElement.getAttribute(MEASURE));

		return vp;
	}

	private GrowingTips getGrowingTipsFromElement(Element fElement) {
		GrowingTips gt = new GrowingTips();
		Element gtElement = (Element) fElement.getElementsByTagName(GROWING_TIPS).item(0);

		gt.setTempreture(Integer.parseInt(gtElement.getElementsByTagName(TEMPRETURE).item(0).getTextContent()));
		gt.setWatering(Integer.parseInt(gtElement.getElementsByTagName(WATERING).item(0).getTextContent()));

		Element tElement = (Element) gtElement.getElementsByTagName(TEMPRETURE).item(0);
		gt.setTempretureMeasure(tElement.getAttribute(MEASURE));
		Element lElement = (Element) gtElement.getElementsByTagName(LIGHTING).item(0);
		gt.setLightRequiring(lElement.getAttribute(LIGHT_REQUIRING));
		Element wElement = (Element) gtElement.getElementsByTagName(WATERING).item(0);
		gt.setWateringMeasure(wElement.getAttribute(MEASURE));

		return gt;
	}

	private String getMultiplyingFromElement(Element fElement) {
		return fElement.getElementsByTagName(MULTIPLYING).item(0).getTextContent();
	}


	private void validate(Document document) throws SAXException, IOException {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Source schemaFile = new StreamSource(new File(xsdFileName));
		Schema schema = factory.newSchema(schemaFile);

		Validator validator = schema.newValidator();
		validator.validate(new DOMSource(document));
	}


}
