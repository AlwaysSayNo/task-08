package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.dom.DOMController;
import com.epam.rd.java.basic.task8.controller.sax.SAXController;
import com.epam.rd.java.basic.task8.controller.stax.STAXController;
import com.epam.rd.java.basic.task8.entities.Flower;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.err.println("input.xml ==> " + xmlFileName);

		domCycle(xmlFileName);
		saxCycle(xmlFileName);
		staxCycle(xmlFileName);
	}

	private static void domCycle(String xmlFileName) throws ParserConfigurationException, IOException, SAXException {
		DOMController domController = new DOMController(xmlFileName);
		domCycleHelper(domController);
	}

	private static void domCycleHelper(DOMController domController) throws ParserConfigurationException, IOException, SAXException {
		List<Flower> flowers = domController.parseFlowers();

		System.err.println("\n\n" + "By name");

		SortController.sortFlowersByName(flowers);

		String outputXmlFile = "output.dom.xml";
		DOMController.writeFlowers(flowers, outputXmlFile);
	}

	private static void saxCycle(String xmlFileName) throws ParserConfigurationException, SAXNotSupportedException, SAXNotRecognizedException {
		SAXController saxController = new SAXController(xmlFileName);
		saxCycleHelper(saxController);
	}

	private static void saxCycleHelper(SAXController saxController) throws ParserConfigurationException, SAXNotSupportedException, SAXNotRecognizedException {
		List<Flower> flowers = saxController.parseFlowers();

		System.err.println("\n\n" + "By origin");

		SortController.sortFlowersByOrigin(flowers);

		String outputXmlFile = "output.sax.xml";
		saxController.writeFlowers(flowers, outputXmlFile);
	}

	private static void staxCycle(String xmlFileName) throws ParserConfigurationException {
		STAXController staxController = new STAXController(xmlFileName);
		staxCycleHelper(staxController);
	}

	private static void staxCycleHelper(STAXController staxController) throws ParserConfigurationException {
		List<Flower> flowers = staxController.parseFlowersCursor();

		System.err.println("\n\n" + "By soil");

		SortController.sortFlowersBySoil(flowers);

		String outputXmlFile = "output.stax.xml";
		STAXController.writeFlowers(flowers, outputXmlFile);
	}

}
