package com.epam.rd.java.basic.task8.entities;

public class VisualParameters {

    private String stemColour;
    private String leafColour;
    private int aveLenFlower;

    private String aveLenFlowerMeasure;

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public String getAveLenFlowerMeasure() {
        return aveLenFlowerMeasure;
    }

    public void setAveLenFlowerMeasure(String aveLenFlowerMeasure) {
        this.aveLenFlowerMeasure = aveLenFlowerMeasure;
    }

    @Override
    public String toString() {
        return "{" + '\n' +
                "stemColour: " + stemColour + '\n' +
                "leafColour: " + leafColour + '\n' +
                "aveLenFlower: " + aveLenFlower +
                ", measure: " + aveLenFlowerMeasure + '\n' +
                "}";
    }
}
