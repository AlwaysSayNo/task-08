package com.epam.rd.java.basic.task8.controller.sax;

import com.epam.rd.java.basic.task8.controller.dom.DOMController;
import com.epam.rd.java.basic.task8.entities.Flower;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

    public List<Flower> parseFlowers() throws SAXNotSupportedException, SAXNotRecognizedException, ParserConfigurationException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		List<Flower> flowers = null;
		try{
			SAXParser saxParser = factory.newSAXParser();

			MyHandler handler = new MyHandler();
			saxParser.parse(xmlFileName, handler);

			flowers = handler.getResult();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		return flowers;
	}

	public void writeFlowers(List<Flower> flowers, String outputXmlFile) throws ParserConfigurationException {
		DOMController.writeFlowers(flowers, outputXmlFile);
	}

}