package com.epam.rd.java.basic.task8.entities;

public class GrowingTips {

    private int tempreture;
    private int watering;

    private String tempretureMeasure;
    private String wateringMeasure;
    private String lightRequiring;

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public String getTempretureMeasure() {
        return tempretureMeasure;
    }

    public void setTempretureMeasure(String tempretureMeasure) {
        this.tempretureMeasure = tempretureMeasure;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    @Override
    public String toString() {
        return "{" + '\n' +
                "tempreture: " + tempreture +
                ", measure: " + tempretureMeasure + '\n' +
                "lightRequiring: " + lightRequiring + '\n' +
                "watering: " + watering +
                ", measure: " + wateringMeasure + '\n' +
                "}";
    }
}
