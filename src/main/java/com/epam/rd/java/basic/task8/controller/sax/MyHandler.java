package com.epam.rd.java.basic.task8.controller.sax;

import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MyHandler extends DefaultHandler {

    private List<Flower> result;
    private Flower currentFlower;
    private StringBuilder currentValue;

    private final String FLOWER = "flower", NAME = "name", SOIL = "soil", ORIGIN = "origin",
            VISUAL_PARAMETERS = "visualParameters", STEM_COLOUR = "stemColour", LEAF_COLOUR = "leafColour",
            AVE_LEN_FLOWER = "aveLenFlower", GROWING_TIPS = "growingTips", TEMPRETURE = "tempreture",
            LIGHTING = "lighting", WATERING = "watering", MULTIPLYING = "multiplying",
            MEASURE = "measure", LIGHT_REQUIRING = "lightRequiring";

    @Override
    public void startDocument() throws SAXException {
        result = new ArrayList<>();
        currentValue = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentValue.setLength(0);

        if(FLOWER.equals(qName)){
            currentFlower = new Flower();
        }
        else if(VISUAL_PARAMETERS.equals(qName)){
            currentFlower.setVisualParameters(new VisualParameters());
        }
        else if(GROWING_TIPS.equals(qName)){
            currentFlower.setGrowingTips(new GrowingTips());
        }
        else if(AVE_LEN_FLOWER.equals(qName)){
            String measure = attributes.getValue(MEASURE);
            currentFlower.getVisualParameters().setAveLenFlowerMeasure(measure);
        }
        else if(TEMPRETURE.equals(qName)){
            String measure = attributes.getValue(MEASURE);
            currentFlower.getGrowingTips().setTempretureMeasure(measure);
        }
        else if(LIGHTING.equals(qName)){
            String lightRequiring = attributes.getValue(LIGHT_REQUIRING);
            currentFlower.getGrowingTips().setLightRequiring(lightRequiring);
        }
        else if(WATERING.equals(qName)){
            String measure = attributes.getValue(MEASURE);
            currentFlower.getGrowingTips().setWateringMeasure(measure);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(FLOWER.equals(qName)){
            if(result == null){
                result = new ArrayList<>();
            }
            result.add(currentFlower);
            currentFlower = null;
        }
        else if(NAME.equals(qName)){
            currentFlower.setName(currentValue.toString());
        }
        else if(SOIL.equals(qName)){
            currentFlower.setSoil(currentValue.toString());
        }
        else if(ORIGIN.equals(qName)){
            currentFlower.setOrigin(currentValue.toString());
        }
        else if(MULTIPLYING.equals(qName)){
            currentFlower.setMultiplying(currentValue.toString());
        }
        else if(STEM_COLOUR.equals(qName)){
            currentFlower.getVisualParameters().setStemColour(currentValue.toString());
        }
        else if(LEAF_COLOUR.equals(qName)){
            currentFlower.getVisualParameters().setLeafColour(currentValue.toString());
        }
        else if(AVE_LEN_FLOWER.equals(qName)){
            currentFlower.getVisualParameters().setAveLenFlower(Integer.parseInt(currentValue.toString()));
        }
        else if(TEMPRETURE.equals(qName)){
            currentFlower.getGrowingTips().setTempreture(Integer.parseInt(currentValue.toString()));
        }
        else if(WATERING.equals(qName)){
            currentFlower.getGrowingTips().setWatering(Integer.parseInt(currentValue.toString()));
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        currentValue.append(ch, start, length);
    }

    public List<Flower> getResult(){
        return result;
    }
}
