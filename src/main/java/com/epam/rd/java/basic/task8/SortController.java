package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.entities.Flower;

import java.util.Comparator;
import java.util.List;

public class SortController {

    private SortController(){}

    public static void sortFlowersByName(List<Flower> flowers) {
        flowers.sort(Comparator.comparing(Flower::getName));
    }

    public static void sortFlowersByOrigin(List<Flower> flowers) {
        flowers.sort(Comparator.comparing(Flower::getOrigin));
    }

    public static void sortFlowersBySoil(List<Flower> flowers) {
        flowers.sort(Comparator.comparing(Flower::getSoil));
    }
}
