package com.epam.rd.java.basic.task8.controller.stax;

import com.epam.rd.java.basic.task8.controller.dom.DOMController;
import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private Flower currentFlower;
	private List<Flower> result;

	private static final String FLOWER = "flower", NAME = "name", SOIL = "soil", ORIGIN = "origin",
			VISUAL_PARAMETERS = "visualParameters", STEM_COLOUR = "stemColour", LEAF_COLOUR = "leafColour",
			AVE_LEN_FLOWER = "aveLenFlower", GROWING_TIPS = "growingTips", TEMPRETURE = "tempreture",
			LIGHTING = "lighting", WATERING = "watering", MULTIPLYING = "multiplying",
			MEASURE = "measure", LIGHT_REQUIRING = "lightRequiring";

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public static void writeFlowers(List<Flower> flowers, String outputXmlFile) throws ParserConfigurationException {
		DOMController.writeFlowers(flowers, outputXmlFile);
	}

	public List<Flower> parseFlowersCursor() {
		result = null;
		try {
			result = parseHelper(Paths.get(xmlFileName));
		} catch (FileNotFoundException | XMLStreamException e) {
			e.printStackTrace();
		}
		return result;
	}

	private List<Flower> parseHelper(Path path) throws FileNotFoundException, XMLStreamException {
		XMLInputFactory factory = XMLInputFactory.newFactory();
		XMLStreamReader reader = factory.createXMLStreamReader(
				new FileInputStream(path.toFile())
		);

		while(reader.hasNext()){
			int eventType = reader.next();

			if(eventType == XMLStreamConstants.START_ELEMENT){
				startElement(reader, eventType);
			}
			else if(eventType == XMLStreamConstants.END_ELEMENT){
				endElement(reader);
			}
		}

		return result;
	}

	private void startElement(XMLStreamReader reader, int eventType) throws XMLStreamException {
		switch (reader.getName().getLocalPart()){

			case FLOWER:
			currentFlower = new Flower();
			break;

			case VISUAL_PARAMETERS:
			currentFlower.setVisualParameters(new VisualParameters());
			break;
			case GROWING_TIPS:
			currentFlower.setGrowingTips(new GrowingTips());
			break;

			case NAME:
			eventType = reader.next();
			if(eventType == XMLStreamConstants.CHARACTERS){
				String name = reader.getText();
				currentFlower.setName(name);
			}
			break;
			case SOIL:
			eventType = reader.next();
			if(eventType == XMLStreamConstants.CHARACTERS){
				String soil = reader.getText();
				currentFlower.setSoil(soil);
			}
			break;
			case ORIGIN:
			eventType = reader.next();
			if(eventType == XMLStreamConstants.CHARACTERS){
				String origin = reader.getText();
				currentFlower.setOrigin(origin);
			}
			break;
			case STEM_COLOUR:
			eventType = reader.next();
			if(eventType == XMLStreamConstants.CHARACTERS){
				String stemColor = reader.getText();
				currentFlower.getVisualParameters().setStemColour(stemColor);
			}
			break;
			case LEAF_COLOUR:
			eventType = reader.next();
			if(eventType == XMLStreamConstants.CHARACTERS){
				String leafColour = reader.getText();
				currentFlower.getVisualParameters().setLeafColour(leafColour);
			}
			break;
			case AVE_LEN_FLOWER:
			String aveLenFlowerMeasure = reader.getAttributeValue(null, MEASURE);
			currentFlower.getVisualParameters().setAveLenFlowerMeasure(aveLenFlowerMeasure);

			eventType = reader.next();
			if(eventType == XMLStreamConstants.CHARACTERS){
				int aveLenFlowers = Integer.parseInt(reader.getText());
				currentFlower.getVisualParameters().setAveLenFlower(aveLenFlowers);
			}
			break;
			case TEMPRETURE:
			String tempretureMeasure = reader.getAttributeValue(null, MEASURE);
			currentFlower.getGrowingTips().setTempretureMeasure(tempretureMeasure);

			eventType = reader.next();
			if(eventType == XMLStreamConstants.CHARACTERS){
				int tempreture = Integer.parseInt(reader.getText());
				currentFlower.getGrowingTips().setTempreture(tempreture);
			}
			break;
			case LIGHTING:
			String lightRequiring = reader.getAttributeValue(null, LIGHT_REQUIRING);
			currentFlower.getGrowingTips().setLightRequiring(lightRequiring);
			break;
			case WATERING:
			String wateringMeasure = reader.getAttributeValue(null, MEASURE);
			currentFlower.getGrowingTips().setWateringMeasure(wateringMeasure);

			eventType = reader.next();
			if(eventType == XMLStreamConstants.CHARACTERS){
				int watering = Integer.parseInt(reader.getText());
				currentFlower.getGrowingTips().setWatering(watering);
			}
			break;
			case MULTIPLYING:
			eventType = reader.next();
			if(eventType == XMLStreamConstants.CHARACTERS){
				String multiplying = reader.getText();
				currentFlower.setMultiplying(multiplying);
			}
			break;

		}

	}

	private void endElement(XMLStreamReader reader){
		if (FLOWER.equals(reader.getName().getLocalPart())) {
			if (result == null) result = new ArrayList<>();
			result.add(currentFlower);
		}
	}

}